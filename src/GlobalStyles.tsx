import styled from "styled-components";

export const Ul = styled.ul`
  display: flex;
  flex-direction: row;

  li {
    margin: 0 20px 0 0;
    list-style-type: none;
    display: flex;
    align-items: center;
  }

  a {
    text-decoration:  none;
    color: #fff;
  }
`;

export const Nav = styled.nav`
  display: flex;
  flex-direction: row;
  align-items: center;
  font-size: 14px;
  line-height: 1.5em;
`;
