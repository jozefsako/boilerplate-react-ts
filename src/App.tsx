import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";

import logo from "./assets/svg/react.svg";
import "./App.css";

import { Nav, Ul } from './GlobalStyles';
import { LoginPage } from "./loadable";


function App() {
  return (
    <div className="App">
      <Router>
        <header className="App-header">
          <Nav>
            <Ul>
              <li>
                <Link to="/">Home</Link>
              </li>

              <li>
                <Link to="/login">Login</Link>
              </li>

              <li>
                <img src={logo} className="App-logo" alt="logo" />
              </li>
            </Ul>
          </Nav>
        </header>

        <Switch>
          <Route path="/login" exact component={LoginPage} />
          <Route path="/" exact />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
