import React, { useEffect, useState } from "react";
import { Loading } from "../../components/Loading";
import { Card, Container, InputContainer, Title, Input, ButtonsContainer, PrimaryButton, StyledImg } from "./loginPage.styles";

interface LoginPageProps { }

const LoginPage = (props: LoginPageProps) => {
  const [email, setEmail] = useState<string>();
  const [password, setPassword] = useState<string>();
  const [login, setLoginPage] = useState<boolean>(false);
  const [user, setUser] = useState<any>();

  const handleClick = (e: any) => {
    e.preventDefault();
    setLoginPage(!login);
  };

  const handleChange = (e: any) => {
    e.preventDefault();
    if (e.target.name === "email") {
      setEmail(e.target.value);
    } else {
      setPassword(e.target.value);
    }
  };

  const handleSubmit = (e: any) => {
    e.preventDefault();
    if (email) {
      const requestOptions = {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({ email: email }),
      };

      fetch("http://localhost:3010/users/whoami", requestOptions)
        .then((data) => {
          return data.json();
        })
        .then((result) => {
          setUser(result);
        });
    }
  };

  console.log(user);
  return (
    <Card>
      {/* <Loading /> */}
      <Container>
        <InputContainer>
          <Title>{login ? "Login" : "Create account"}</Title>

          <Input
            type="text"
            name="email"
            placeholder="Your Email"
            onChange={handleChange}
            required
          />
          <Input
            type="password"
            name="password"
            placeholder="Your Password"
            onChange={handleChange}
            required
          />

          <ButtonsContainer>
            <PrimaryButton onClick={handleSubmit}>
              {login ? "Login" : "Sign-up"}
            </PrimaryButton>
          </ButtonsContainer>

          <span style={{ fontSize: "14px" }}>
            {login ? (
              <>
                Dont have an account ?
                <a onClick={handleClick}>
                  <strong> Sign-up in here</strong>
                </a>
              </>
            ) : (
              <>
                Already have an account ?
                <a onClick={handleClick}>
                  <strong> Log-in here</strong>
                </a>
              </>
            )}
          </span>
        </InputContainer>
        <StyledImg />
      </Container>
    </Card>
  );
};


export default LoginPage;