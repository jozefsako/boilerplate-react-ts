import styled, { css } from "styled-components";

export const Card = styled.div`
  border-radius: 15px;
  width: 100%;
  max-width: 1200px;
  height: 800px;
  background: #fff;
  margin: 100px auto;
`;

export const Container = styled.div`
  display: flex;
  flex-direction: row;
  overflow: hidden;
`;

export const StyledImg = styled.div`
  height: 800px;
  max-width: 600px;
  width: 100%;
  border-radius: 0 15px 15px 0;
  background-image: url("https://unsplash.it/1200");
  background-size: cover;
  background-position: center;
  background-repeat: no-repeat;
`;

export const Input = styled.input`
  padding: 10px 15px;
  line-height: 24px;
  margin: 0 0 20px 0;
`;

export const ButtonMixing = css`
  font-weight: 400;
  text-align: center;
  white-space: nowrap;
  vertical-align: middle;
  border: 1px solid transparent;
  padding: 10px 16px;
  font-size: 14px;
  line-height: 1.42857143;
  border-radius: 10px;
  width: 100%;
`;

export const PrimaryButton = styled.button`
  ${ButtonMixing};
  color: #fff;
  background: #1a1a1a;
  border-color: #1a1a1a;
`;

export const Title = styled.span`
  width: 100%;
  display: block;
  font-size: 30px;
  color: #555;
  line-height: 1.2;
  text-align: center;
  margin: 0 0 40px 0;
`;

export const ButtonsContainer = styled.div`
  display: flex;
  flex-direction: row;
  margin: 20px 0 60px 0;
`;

export const InputContainer = styled.div`
  display: flex;
  flex-direction: column;
  padding: 40px 60px;
  justify-content: center;
  width: 50%;
`;
