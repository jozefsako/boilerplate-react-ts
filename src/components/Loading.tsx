import "./loading.css";

interface LoadingProps {
	color?: string;
}

export const Loading = () => (
	<svg
		viewBox="0 0 24 24"
		xmlns="<http://www.w3.org/2000/svg>"
	>
		<circle
			r="8"
			className="main"
			cx="12"
			cy="12"
			fill="none"
			stroke-width="4"
			stroke={"tomato"}
		/>
	</svg>
)
