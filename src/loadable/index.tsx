
import Loadable from 'react-loadable';
import { Loading } from "../components/Loading"

export const LoginPage = Loadable({ loader: () => import('../pages/Login/LoginPage'), loading: Loading })
